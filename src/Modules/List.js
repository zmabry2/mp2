import React, { Component } from 'react';
import '../App.scss';
import Pokemon from './Pokemon';
import PropTypes from 'prop-types';
import {
  Link
} from "react-router-dom";
import axios from 'axios';
class List extends Component {
      constructor(props){
          super(props);
          this.state = {data: [], table: [], key: props.sort, rev: props.rev}
      }

      componentDidMount() {
        for (let i = 0; i <= 151; i++) {
            axios.get("https://pokeapi.co/api/v2/pokemon/"+i+"/").then(res => {
                this.setState({data: this.state.data.concat(new Pokemon(res.data)), table: this.state.table.concat(new Pokemon(res.data)),key: "id", rev:true, load: i});
            });
        }

      }

      renderList(key, rev) {

          return this.state.table.map(curr => {
            return (
                <tr>
                    <td>{curr.id}</td>
                    <Link to={'/mp2/detail/'+curr.id}><td><img src={curr.sprite} alt=""/></td></Link>
                    <td>{curr.name}</td>
                    <td>{curr.height}</td>
                    <td>{curr.weight}</td>
                    <td>{curr.type}</td>
                </tr>
            )
          })
      }
      update(k, r) {
        if(k === this.state.key) {
            if(this.state.rev === true) {
                r = false;
            } else {
                r = true;
            }
        }
        let set;
        switch(k) {
        case "id":
            set = this.state.table.sort((a,b) => (a.id > b.id) ? 1 : -1);
            break;
        case "name":
            set = this.state.table.sort((a,b) => (a.name > b.name) ? 1 : -1);
            break;
        case "height":
            set = this.state.table.sort((a,b) => (a.height > b.height) ? 1 : -1);
            break;
        case "weight":
            set = this.state.table.sort((a,b) => (a.weight > b.weight) ? 1 : -1);
            break;
        case "type":
            set = this.state.table.sort((a,b) => (a.type > b.type) ? 1 : -1);
            break;
        default:
            set = this.state.table;
        }
        if (r === true) {
            set = set.reverse();
        }
        this.setState({data: this.state.data, table: set, key: k, rev: r, load: this.state.load});
      }
      filter() {
          let res = [];
          for(let i = 0; i < this.state.data.length; i++) {
             if(this.state.data[i].name.includes(this.query.value)) {
                 res.push(this.state.data[i])
             }
          }
          this.setState({data: this.state.data, table: res, key: this.state.key, rev: this.state.rev, load: this.state.load})
      }
      render() {
        return (
                <div className="App">
                <header className="App-header">
                    <h5>Click on a column (besides sprite) to sort ascending, click it again for descending.<br/>
                        Click on a sprite for a detailed view.</h5>
                    <form onSubmit={this.filter}>
                    <input className="query" type="text" placeholder="Type partial or full pokemon name here..." ref={(query) => this.query = query} value={this.props.query} onChange={this.filter.bind(this)}/>
                    </form>
                    <tbody>
                        <tr>
                            <th onClick={this.update.bind(this, "id", false)}>ID</th>
                            <th>Sprite</th>
                            <th onClick={this.update.bind(this, "name", false)}>Name</th>
                            <th onClick={this.update.bind(this, "height", false)}>Height</th>
                            <th onClick={this.update.bind(this, "weight", false)}>Weight</th>
                            <th onClick={this.update.bind(this, "type", false)}>Type</th>
                        </tr>
                        {this.renderList(this.key, this.rev)}
                    </tbody>
                </header>
                </div>
            );
        }
    }

List.defaultProps = {
    sort: "id",
    rev: true
}
List.propTypes = {
    sort: PropTypes.string.isRequired,
    rev: PropTypes.bool.isRequired
}
export default List;