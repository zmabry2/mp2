class Pokemon {
    capitalize(str) {
        return (str === undefined ? "":str.charAt(0).toUpperCase() + str.slice(1));
    }
    constructor(data) {
        if (data === undefined) {
            this.name = "Pikachu"
            this.id = 25;
            this.height = 4;
            this.weight = 60;
            this.sprite = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png";
            this.type = "electric";
        } else {
            //alert(Object.keys(data))
            this.name = this.capitalize(data.name);
            this.id = data.id;
            this.height = data.height;
            this.weight = data.weight;
            if (data.types === undefined) {                
                this.name = "Pikachu"
                this.id = 25;
                this.height = 4;
                this.weight = 60;
                this.sprite = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png";
                this.type = "electric";
                this.type2 = '';
            } else {
                this.sprite = data.sprites["front_default"];
                this.shiny = data.sprites["front_shiny"];
                this.type = this.capitalize(data.types ? data.types[data.types.length-1].type.name : '');
                this.type2 = this.capitalize(data.types[data.types.length-2] ? data.types[data.types.length-2].type.name : '');
            }

        }
    }
}

export default Pokemon;