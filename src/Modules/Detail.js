import React, { Component } from 'react';
import '../App.scss';
import Pokemon from './Pokemon';
import {
  Link,
  withRouter
} from "react-router-dom";
import axios from 'axios';
class Detail extends Component {
      constructor(){
          super(); 
          this.state = {data: [], id: 0}
      }

      componentDidMount() {
        let id = this.props.match.params.id;
        axios.get('https://pokeapi.co/api/v2/pokemon/'+id+'/').then(res => {
            this.setState({data: res.data, id: id});
        });
      }

      componentDidUpdate() {
        let id = this.props.match.params.id;
        axios.get('https://pokeapi.co/api/v2/pokemon/'+id+'/').then(res => {
            this.setState({data: res.data, id: id});
        });
      }
      render() {
        let pokemon = new Pokemon(this.state.data);
        return (
                <div className="App">
                <header className="App-header-horizontal">
                        <Link to={"/mp2/detail/"+(parseInt(this.state.id, 10) - 1)}>
                            <div className="back">Back</div>
                        </Link>
                        <div className="grid-parent">
                            <div className="grid-image"><img className="pokesprite" src={pokemon.sprite} alt="alt"/><img className="pokesprite" src={pokemon.shiny} alt="alt"/></div>
                            <div className="grid-info">Name: {pokemon.name}</div>
                            <div className="grid-info">ID: {pokemon.id}</div>
                            <div className="grid-info">Height: {pokemon.height}</div>
                            <div className="grid-info">Weight: {pokemon.weight}</div>
                            <div className="grid-info">Type 1: {pokemon.type}</div>
                            <div className="grid-info">Type 2: {pokemon.type2.length === 0 ? "None" : pokemon.type2}</div>
                        </div>
                        <Link to={"/mp2/detail/"+(parseInt(this.state.id, 10) + 1)}>
                            <div className="next">Next</div>
                        </Link>
                </header>
                </div>
            );
        }
    }
export default withRouter(Detail);