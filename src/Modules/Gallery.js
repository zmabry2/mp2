import React, { Component } from 'react';
import '../App.scss';
import Pokemon from './Pokemon';
import Bug from './types/type-bug.png';
import Dragon from './types/type-dragon.png';
import Electric from './types/type-electric.png';
import Fight from './types/type-fight.png';
import Fire from './types/type-fire.png';
import Flying from './types/type-flying.png';
import Ghost from './types/type-ghost.png';
import Grass from './types/type-grass.png';
import Ground from './types/type-ground.png';
import Ice from './types/type-ice.png';
import Normal from './types/type-normal.png';
import Poison from './types/type-poison.png';
import Psychic from './types/type-psychic.png';
import Rock from './types/type-rock.png';
import Water from './types/type-water.png';
import PropTypes from 'prop-types';
import {
  Link
} from "react-router-dom";
import axios from 'axios';
class Gallery extends Component {
    constructor(props){
        super(props);
        this.state = {data: [], table: [], key: props.sort, rev: props.rev}
    }

      componentDidMount() {
        for (let i = 0; i <= 151; i++) {
            axios.get("https://pokeapi.co/api/v2/pokemon/"+i+"/").then(res => {
                this.setState({data: this.state.data.concat(new Pokemon(res.data)), table: this.state.table.concat(new Pokemon(res.data)),key: "id", rev:true, load: i});
            });
        }

      }

      renderList(key, rev) {
          let set = this.state.table.sort((a,b) => (a.id > b.id) ? 1 : -1);
          return set.map(curr => {
            return (
                <Link to={'/mp2/detail/'+curr.id}><img className="gallery-tile" src={curr.sprite} alt=""/></Link>
            )
          })
      }
      filter(query) {
          let res = [];
          for(let i = 0; i < this.state.data.length; i++) {
             if(this.state.data[i].type.includes(query) || this.state.data[i].type2.includes(query))  {
                 res.push(this.state.data[i])
             }
          }
          this.setState({data: this.state.data, table: res, key: this.state.key, rev: this.state.rev, load: this.state.load})
      }
      render() {
        return (
                <div className="App">
                <header className="App-header">
                    <div className="clear" onClick={this.filter.bind(this, "")}>Clear Filters</div>
                    <div className="types">
                        <img src={Bug} onClick={this.filter.bind(this, "Bug")} alt=""></img>
                        <img src={Dragon} onClick={this.filter.bind(this, "Dragon")} alt=""></img>
                        <img src={Electric} onClick={this.filter.bind(this, "Electric")} alt=""></img>
                        <img src={Fight} onClick={this.filter.bind(this, "Fight")} alt=""></img>
                        <img src={Fire} onClick={this.filter.bind(this, "Fire")} alt=""></img>
                        <img src={Flying} onClick={this.filter.bind(this, "Flying")} alt=""></img>
                        <img src={Ghost} onClick={this.filter.bind(this, "Ghost")} alt=""></img>
                        <img src={Grass} onClick={this.filter.bind(this, "Grass")} alt=""></img>
                        <img src={Ground} onClick={this.filter.bind(this, "Ground")} alt=""></img>
                        <img src={Ice} onClick={this.filter.bind(this, "Ice")} alt=""></img>
                        <img src={Normal} onClick={this.filter.bind(this, "Normal")} alt=""></img>
                        <img src={Poison} onClick={this.filter.bind(this, "Poison")} alt=""></img>
                        <img src={Psychic} onClick={this.filter.bind(this, "Psychic")} alt=""></img>
                        <img src={Rock} onClick={this.filter.bind(this, "Rock")} alt=""></img>
                        <img src={Water} onClick={this.filter.bind(this, "Water")} alt=""></img>
                    </div>
                    <div className="gallery-parent">
                        {this.renderList("id", false)}
                    </div>
                </header>
                </div>
            );
        }
    }

Gallery.defaultProps = {
    sort: "id",
    rev: true
}
Gallery.propTypes = {
    sort: PropTypes.string.isRequired,
    rev: PropTypes.bool.isRequired
}
export default Gallery;