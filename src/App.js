import React from 'react';
import Detail from './Modules/Detail';
import Gallery from './Modules/Gallery';
import List from './Modules/List';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";
function App() {
  return (
    <Router>
      <div >
        <ul className="navbar">
          <li className="navlink">
            <Link className="navlink" to="/mp2/">List</Link>
          </li>
          <li className="navlink">
            <Link className="navlink" to="/mp2/gallery">Gallery</Link>
          </li>
          <li className="navlink">
            <Link className="navlink" to="/mp2/detail">Detail</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/mp2/gallery">
            <Gallery sort={"id"} rev={true}/>
          </Route>
          <Route path="/mp2/detail">
            <Details />
          </Route>
          <Route path="/mp2/">
            <List sort={false} rev={true}/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Details() {
  let match = useRouteMatch();
  return (
      <Switch>
        <Route path={`${match.path}/:id`}>
          <Detail />
        </Route>
        <Route path={match.path}>
          <Detail />
        </Route>
      </Switch>
  );
}
export default App;
